package com.yu.link;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.yu.link.base.**.mapper")
public class LinkStartApplication {

    public static void main(String[] args) {
        SpringApplication.run(LinkStartApplication.class, args);
    }

}
