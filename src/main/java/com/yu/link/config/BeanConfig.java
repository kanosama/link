package com.yu.link.config;

import com.yu.link.entity.DbInfo;
import com.yu.link.entity.RequestLimit;
import com.yu.link.utils.DbUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig<T> {

    @Value("${spring.datasource.url}")
    private String dbName;


    @Value("${request_limit.time}")
    private long time;
    @Value("${request_limit.count}")
    private int count;
    @Value("${request_limit.waits}")
    private long waits;


    @Bean
    @ConditionalOnMissingBean(name = "dbName")
    public DbInfo dbName(){
        int index = dbName.lastIndexOf("/");
        int index2 = dbName.indexOf("?");
        String substring = dbName.substring(index + 1, index2);
        DbInfo dbInfo = new DbInfo();
        dbInfo.setDbName(substring);
        return dbInfo;
    }


    @Bean
    @ConditionalOnMissingBean(name = "RequestLimit")
    public RequestLimit RequestLimit(){
        RequestLimit build = RequestLimit.builder().count(count).time(time).watis(waits).build();
        return build;
    }



    @Bean
    public DbUtils<T> dbUtils(){
        return new DbUtils();
    }
}
