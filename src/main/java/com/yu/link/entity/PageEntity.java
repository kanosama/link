package com.yu.link.entity;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PageEntity {

    @NotNull(message = "pageNo is null")
    private Integer pageNo;

    @NotNull(message = "pageSize is null")
    private Integer pageSize;

}
