package com.yu.link.entity;

import lombok.Data;

/**
 * <p> Project: link-start - RequestLimitEntity
 * <p> create by ly on 2023-10-24 16:35:50
 *
 * @author ly [bearkuku9696@gmail.com]
 * @version 1.0
 */
@Data
public class RequestLimitEntity {

    private String ip;
    private String url;
    private String firstDateTime;
    private String lastDateTime;
    private long counts;

}
