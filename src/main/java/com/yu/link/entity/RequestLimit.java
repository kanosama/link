package com.yu.link.entity;

import com.yu.link.utils.SpringUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p> Project: link-start - RequestLimit
 * <p> create by ly on 2023-10-24 17:09:36
 *
 * @author ly [bearkuku9696@gmail.com]
 * @version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RequestLimit {
    private long time = 60000;
    private int count = 9999;
    private long watis = 0;
}
