package com.yu.link.entity;

import com.yu.link.utils.SpringUtils;

import javax.swing.*;

/**
 * <p> Project: link-start - DataSource
 * <p> create by ly on 2023-10-26 14:19:39
 *
 * @author ly [bearkuku9696@gmail.com]
 * @version 1.0
 */
public class DataSource {

    public String getDriverClassName(){
        return SpringUtils.getConfig("spring.datasource.driver-class-name");
    }

    public String getUrl(){
        return SpringUtils.getConfig("spring.datasource.url");
    }

    public String getUsername(){
        return SpringUtils.getConfig("spring.datasource.username");
    }

    public String getPssword(){
        return SpringUtils.getConfig("spring.datasource.password");
    }


}
