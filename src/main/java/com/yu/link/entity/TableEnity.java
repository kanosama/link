package com.yu.link.entity;

public class TableEnity {


    private String column_name;  //字段名
    private String data_type;   //字段类型
    private String character_maximum_length;
    private String numeric_precision;
    private String numeric_scale;
    private String is_nullable;
    private String is_auto_increment;
    private String column_default;
    private String column_comment;  //备注
    private String column_type;

    public String getColumn_type() {
        return column_type;
    }

    public void setColumn_type(String column_type) {
        this.column_type = column_type;
    }

    public String getColumn_name() {
        return column_name;
    }

    public void setColumn_name(String column_name) {
        this.column_name = column_name;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public String getCharacter_maximum_length() {
        return character_maximum_length;
    }

    public void setCharacter_maximum_length(String character_maximum_length) {
        this.character_maximum_length = character_maximum_length;
    }

    public String getNumeric_precision() {
        return numeric_precision;
    }

    public void setNumeric_precision(String numeric_precision) {
        this.numeric_precision = numeric_precision;
    }

    public String getNumeric_scale() {
        return numeric_scale;
    }

    public void setNumeric_scale(String numeric_scale) {
        this.numeric_scale = numeric_scale;
    }

    public String getIs_nullable() {
        return is_nullable;
    }

    public void setIs_nullable(String is_nullable) {
        this.is_nullable = is_nullable;
    }

    public String getIs_auto_increment() {
        return is_auto_increment;
    }

    public void setIs_auto_increment(String is_auto_increment) {
        this.is_auto_increment = is_auto_increment;
    }

    public String getColumn_default() {
        return column_default;
    }

    public void setColumn_default(String column_default) {
        this.column_default = column_default;
    }

    public String getColumn_comment() {
        return column_comment;
    }

    public void setColumn_comment(String column_comment) {
        this.column_comment = column_comment;
    }
}
