package com.yu.link.entity;

import lombok.Data;

import java.util.List;

/**
 * @program: erpserver-master-master
 * @description:
 * @author: 李宇
 * @create: 2022-05-18 23:45
 * @remark: ...
 **/
@Data
public class PageResult<T> {

    private List<T> data;

    private long total;

    private long pageCount;


    public PageResult(List<T> data, long total, long pageCount) {
        this.data = data;
        this.total = total;
        this.pageCount = pageCount;
    }
}
