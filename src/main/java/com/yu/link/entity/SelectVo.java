package com.yu.link.entity;

import lombok.Data;

@Data
public class SelectVo {

    private String id;
    private String name;

}
