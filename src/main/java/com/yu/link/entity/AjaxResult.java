package com.yu.link.entity;


public class AjaxResult {

    public Integer code;
    public String msg;
    public Object data;

    public static AjaxResult error(String msg){
        return new AjaxResult(500, msg);
    }

    public static AjaxResult success(String msg){
        return new AjaxResult(200, msg);
    }

    public static AjaxResult success(String msg, Object data){
        return new AjaxResult(200, msg, data);
    }

    public static AjaxResult success(){
        return new AjaxResult(200, "操作成功");
    }

    public static AjaxResult create(Integer code, String msg){
        return new AjaxResult(code, msg);
    }

    public static AjaxResult create(Integer code, String msg, Object data){
        return new AjaxResult(code, msg, data);
    }


    private AjaxResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private AjaxResult(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
}
