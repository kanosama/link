package com.yu.link.entity;

import com.yu.link.utils.SpringUtils;

/**
 * <p> Project: link-start - RSAConfig
 * <p> create by ly on 2023-10-26 10:29:43
 *
 * @author ly [bearkuku9696@gmail.com]
 * @version 1.0
 */
public class RSAConfig {
    public String getPublicKey() {
        return SpringUtils.getConfig("rsa.private-key");
    }
    public String getPrivateKey() {
        return SpringUtils.getConfig("rsa.public-key");
    }
    public Boolean getResponseEncryption() {
        return Boolean.parseBoolean(SpringUtils.getConfig("rsa.response-encryption"));
    }

}
