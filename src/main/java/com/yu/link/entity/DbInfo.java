package com.yu.link.entity;

import lombok.Data;

@Data
public class DbInfo {

    private String dbName;

}
