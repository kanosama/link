package com.yu.link.entity;

import com.yu.link.utils.SpringUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * <p> Project: link-start - AESConfig
 * <p> create by ly on 2023-10-26 09:35:42
 *
 * @author ly [bearkuku9696@gmail.com]
 * @version 1.0
 */

public class AESConfig {
    public String getKey() {
        return SpringUtils.getConfig("aes.key");
    }
    public String getOffset() {
        return SpringUtils.getConfig("aes.offset");
    }
}
