package com.yu.link.utils;

import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @program: erpserver-master-master
 * @description:
 * @author: 李宇
 * @create: 2022-05-17 00:22
 * @remark: ...
 **/
public class StrUtils {


    public static String objectToValue (Object invoke) {
        if (invoke instanceof String) {
            return "'" + invoke.toString() + "'";
        }
        if (invoke instanceof Long) {
            return invoke.toString();
        }
        if (invoke instanceof Integer) {
            return invoke.toString();
        }
        if (invoke instanceof BigDecimal) {
            return invoke.toString();
        }
        if (invoke instanceof Double) {
            return invoke.toString();
        }
        if (invoke instanceof Date) {
            return "'" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format((Date)invoke) + "'";
        }

        return null;
    }


    /**
     * 首字符大写
     * @param value
     * @return
     */
    public static String oneCharUp(String value){
        String s = value;
        String s0 = s.substring(0, 1);
        String s1 = s.substring(1, s.length());
        s0 = s0.toUpperCase();
        return s0 + s1;
    }


    private static Pattern humpPattern = Pattern.compile("[A-Z]");
    /**
     * 驼峰转下划线,最后转为大写
     * @param str
     * @return
     */
    public static String humpToLine(String str) {
        Matcher matcher = humpPattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString().toLowerCase(Locale.ROOT);
    }

    /**
     * 字符串驼峰
     * @return
     */
    public static String lineToHump(String value){
        if (StringUtils.isBlank(value)) {
            return "";
        }
        if (value.indexOf("t_") == 0) {
            value = value.substring(2);
        }
        String[] split = value.split("_");

        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < split.length; i++) {
            if (i == 0) {
                buffer.append(split[i]);
            }else {
                buffer.append(oneCharUp(split[i]));
            }
        }
        return buffer.toString();
    }

    // 使用GZIP进行字符串压缩
    public static String compressString(String input) {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream(input.length());
            GZIPOutputStream gzip = new GZIPOutputStream(bos);
            gzip.write(input.getBytes("UTF-8"));
            gzip.close();
            byte[] compressed = bos.toByteArray();
            bos.close();
            return java.util.Base64.getEncoder().encodeToString(compressed);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // 使用GZIP进行字符串解压缩
    public static String decompressString(String compressedInput) {
        try {
            byte[] compressed = Base64.getDecoder().decode(compressedInput);
            ByteArrayInputStream bis = new ByteArrayInputStream(compressed);
            GZIPInputStream gis = new GZIPInputStream(bis);
            byte[] bytes = new byte[1024];
            StringBuilder result = new StringBuilder();
            int len;
            while ((len = gis.read(bytes)) > 0) {
                result.append(new String(bytes, 0, len, "UTF-8"));
            }
            gis.close();
            bis.close();
            return result.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
