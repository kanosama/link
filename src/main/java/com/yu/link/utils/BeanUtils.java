package com.yu.link.utils;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * @program: erpserver-master-master
 * @description:
 * @author: 李宇
 * @create: 2022-05-28 17:56
 * @remark: ...
 **/
public class BeanUtils<T> {

    /**
     * 复制对象到另一个对象
     *
     * @param clazz
     * @return
     */
    public static <T> T copy(Object o, Class<T> clazz) {
        return JSONObject.parseObject(JSONObject.toJSONString(o), clazz);
    }

    /**
     * 复制对象list到另一个list
     *
     * @param list
     * @param clazz
     * @return
     */
    public static <T> List<T> copyList(List<Object> list, Class<T> clazz) {
        String s = JSONObject.toJSONString(list);
        JSONArray objects = JSONArray.parseArray(s);
        return objects.toJavaList(clazz);
    }


    public static <T> T copyMap(Map<String, Object> map, Class<T> clazz) {
        return JSONObject.parseObject(JSONObject.toJSONString(map), clazz);
    }

    public static <T> List<T> copyListMap(List<Map<String, Object>> list, Class<T> clazz) {
        return JSONArray.parseArray(JSONObject.toJSONString(list)).toJavaList(clazz);
    }


    public static <T> T convert(Object obj, Class<T> clazz) {
        return obj == null ? null : (T) obj;
    }
}
