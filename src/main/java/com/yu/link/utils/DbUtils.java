package com.yu.link.utils;

import com.alibaba.fastjson2.JSONObject;
import com.yu.link.entity.DbInfo;
import com.yu.link.entity.PageResult;
import com.yu.link.annotations.Table;
import com.yu.link.entity.TableEnity;
import com.yu.link.annotations.TableInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

/**
 * @program: erpserver-master-master
 * @description: CRUD工具类
 * @author: 李宇
 * @create: 2022-05-17 02:21
 * @remark: ...
 **/
public class DbUtils<T>{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DbInfo dbInfo;

    private final Logger logger = LoggerFactory.getLogger(getClass());


    public long findNumber(String sql ){
        logger.info(sql);
        Map<String, Object> stringObjectMap = findOne(sql);
        long number = 0;
        for (Map.Entry<String, Object> entry : stringObjectMap.entrySet()) {
            number = (long) entry.getValue();
        }
        return number;
    }

    public Long findNumber(String sql , Object[] objects){
        logger.info(sql);
        logger.info("data = >> " + JSONObject.toJSONString(objects));
        Map<String, Object> stringObjectMap = findOne(sql, objects);
        long number = 0;
        for (Map.Entry<String, Object> entry : stringObjectMap.entrySet()) {
            number = Long.parseLong(String.valueOf(entry.getValue()));
        }
        return number;
    }

    public List<Map<String, Object>> findListMapPage(String sql , Object[] objs , Integer pageNo, Integer pageSize){
        pageNo--;
        sql += "\nlimit " + (pageNo * pageSize) + " , " + pageSize;
        return findList(sql, objs);
    }

    public PageResult findListPage(String sql , Object[] objs , Class<T> clazz, Integer pageNo, Integer pageSize){
        if (objs.length == 0) {
            return findListPage(sql, clazz, pageNo, pageSize);
        }

        pageNo--;

        String countSql = "select count(1) from ( " + sql + " )t ";
        logger.info(countSql, objs);
        long number = findNumber(countSql, objs);
        sql += "\nlimit " + (pageNo * pageSize) + " , " + pageSize;
        logger.info(sql, objs);
        List<T> list = findList(sql, objs, clazz);

        long pageCount = (number + pageSize - 1) / pageSize;

        return new PageResult<T>( list, number, pageCount);
    }


    public PageResult findListPage(String sql , Class<T> clazz, Integer pageNo, Integer pageSize){
        pageNo--;
        String countSql = "select count(1) from ( " + sql + " )t ";
        logger.info(countSql);
        long number = findNumber(countSql);
        sql += "\nlimit " + (pageNo * pageSize) + " , " + pageSize;
        logger.info(sql);
        List<T> list = findList(sql, clazz);
        long pageCount = (number + pageSize - 1) / pageSize;
        return new PageResult<T>(list, number, pageCount);
    }


    public int insertList(List<T> list, Class clazz) throws Exception {
        Annotation annotation = clazz.getAnnotation(Table.class);
        Table table = (Table) annotation;
        String tableName = table.tableName();
        String sql = "insert into " + tableName + " (";

        Field[] fields = clazz.getDeclaredFields();
        boolean flag = false;
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            if (field.isAnnotationPresent(TableInfo.class)) {
                TableInfo tableInfo = field.getDeclaredAnnotation(TableInfo.class);
                String columnComment = tableInfo.columnComment();
                String columnName = tableInfo.columnName();
                String columnType = tableInfo.columnType();
                int autoIncrement = tableInfo.autoIncrement();
                if (autoIncrement == 1) {
                    continue;
                }
                if (flag) {
                    sql +=  "," + columnName;
                }else {
                    flag = true;
                    sql += columnName;
                }
            }
        }

        sql += ") values (";
        flag = false;
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            if (field.isAnnotationPresent(TableInfo.class)) {
                TableInfo tableInfo = field.getDeclaredAnnotation(TableInfo.class);
                int autoIncrement = tableInfo.autoIncrement();
                Method method = clazz.getDeclaredMethod("get" + StrUtils.oneCharUp(field.getName()));
                if (autoIncrement == 1) {
                    continue;
                }
                if (flag) {
                    sql += ", ? " ;
                }else {
                    flag = true;
                    sql += " ? ";
                }
            }
        }
        sql += ")";

        logger.info(sql);
        int[] ints = jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                T t = list.get(i);
                Class tClass = t.getClass();
                Field[] fs = tClass.getDeclaredFields();

                List<Field> declaredFields = new ArrayList<>();
                for (Field field : fs) {
                    if (field.isAnnotationPresent(TableInfo.class)) {
                        TableInfo annotation1 = field.getAnnotation(TableInfo.class);
                        int i1 = annotation1.autoIncrement();
                        if (i1 == 1) {
                            continue;
                        }else {
                            declaredFields.add(field);
                        }
                    }
                }



                for (int j = 0; j < declaredFields.size(); j++) {
                    Field field = declaredFields.get(j);

                    Method declaredMethod = null;
                    try {
                        declaredMethod = tClass.getDeclaredMethod("get" + StrUtils.oneCharUp(field.getName()));
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                    Object invoke = null;
                    try {
                        invoke = declaredMethod.invoke(t);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                    if (invoke instanceof String) {
                        preparedStatement.setString(j + 1, invoke.toString());
                    } else if (invoke instanceof Integer) {
                        preparedStatement.setInt(j + 1, Integer.parseInt(invoke.toString()));
                    } else if (invoke instanceof Long) {
                        preparedStatement.setLong(j + 1, Long.parseLong(invoke.toString()));
                    } else if (invoke instanceof Double) {
                        preparedStatement.setDouble(j + 1, Double.parseDouble(invoke.toString()));
                    } else if (invoke instanceof BigDecimal) {
                        preparedStatement.setBigDecimal(j + 1, new BigDecimal(invoke.toString()));
                    } else if (invoke instanceof Date) {
                        preparedStatement.setDate(j + 1, (java.sql.Date) invoke);
                    } else {
                        if (invoke == null) {
                            preparedStatement.setObject(j + 1, null);
                        }else {
                            preparedStatement.setString(j + 1, invoke.toString());
                        }

                    }
                }
            }

            @Override
            public int getBatchSize() {
                return list.size();
            }
        });
        return 1;
    }


    public List<T> findList(String sql  , Class<T> clazz){
        logger.info(sql);
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);

        return getTs((Class<T>) clazz, maps);
    }
    public List<T> findList(String sql , Object[] objects , Class<T> clazz){
        logger.info(sql);
        logger.info("data = >> " + JSONObject.toJSONString(objects));
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql, objects);

        return getTs((Class<T>) clazz, maps);
    }


    public List<String> findStringList(String sql , Object[] objects){
        logger.info(sql);
        logger.info("data = >> " + JSONObject.toJSONString(objects));
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql, objects);

        ArrayList<String> strings = new ArrayList<>();
        for (Map<String, Object> map : maps) {
            String value = "";
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                value = entry.getValue().toString();
                strings.add(value);
                break;
            }
        }
        return strings;
    }

    private List<T> getTs(Class<T> clazz, List<Map<String, Object>> maps) {
        List<Map<String, Object>> result = new ArrayList<>();
        for (Map<String, Object> map : maps) {
            Map<String, Object> resultMap = new HashMap<>();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String key = entry.getKey();
                String s = StrUtils.lineToHump(key);
                resultMap.put(s, entry.getValue());
            }
            result.add(resultMap);
        }
        return BeanUtils.copyListMap(result, clazz);
    }

    public T findOne(String sql , Object[] objects , Class<T> calzz){
        logger.info(sql);
        sql = "select * from (" + sql + ") t limit 1";
        logger.info("data = >> " + JSONObject.toJSONString(objects));
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql, objects);

        if (maps.size() == 0) {
            return null;
        }
        List<T> ts = getTs((Class<T>) calzz, maps);
        return BeanUtils.copy(ts.get(0), calzz);
    }
    public T findOne(String sql , Class<T> calzz){
        logger.info(sql);
        sql = "select * from (" + sql + ") t limit 1";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);
        if (maps.size() == 0) {
            return null;
        }
        List<T> ts = getTs((Class<T>) calzz, maps);
        return BeanUtils.copy(ts.get(0), calzz);
    }
    public Map<String, Object> findOne(String sql){
        logger.info(sql);
        sql = "select * from (" + sql + ") t limit 1";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);
        if (maps.size() == 0) {
            return null;
        }
        return maps.get(0);
    }
    public Map<String, Object> findOne(String sql, Object[] objects){
        logger.info(sql);
        sql = "select * from (" + sql + ") t limit 1";
        logger.info("data = >> " + JSONObject.toJSONString(objects));
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql, objects);
        if (maps.size() == 0) {
            return null;
        }
        List<Map<String, Object>> result = new ArrayList<>();
        for (Map<String, Object> map : maps) {
            Map<String, Object> resultMap = new HashMap<>();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String key = entry.getKey();
                String s = StrUtils.lineToHump(key);
                resultMap.put(s, entry.getValue());
            }
            result.add(resultMap);
        }
        return result.get(0);
    }
    public List<Map<String, Object>> findList(String sql, Object[] objects){
        logger.info(sql);
        logger.info("data = >> " + JSONObject.toJSONString(objects));
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql, objects);
        List<Map<String, Object>> result = new ArrayList<>();
        for (Map<String, Object> map : maps) {
            Map<String, Object> resultMap = new HashMap<>();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String key = entry.getKey();
                String s = StrUtils.lineToHump(key);
                resultMap.put(s, entry.getValue());
            }
            result.add(resultMap);
        }
        return result;
    }

    public List<Map<String, Object>> findList(String sql){
        logger.info(sql);
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);
        List<Map<String, Object>> result = new ArrayList<>();
        for (Map<String, Object> map : maps) {
            Map<String, Object> resultMap = new HashMap<>();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String key = entry.getKey();
                String s = StrUtils.lineToHump(key);
                resultMap.put(s, entry.getValue());
            }
            result.add(resultMap);
        }
        return result;
    }


    public int update(String sql , Object[] objects){
        logger.info(sql);
        logger.info("data = >> " + JSONObject.toJSONString(objects));
        return jdbcTemplate.update(sql, objects);
    }

    public int update(String sql ){
        return jdbcTemplate.update(sql, new Object[]{});
    }


    public int updateById(T t, String tableName) throws  Exception{
        List<TableEnity> tableEnitieList = getTableInfo(tableName);
        String sql = "update " + tableName + " set ";

        Class clazz = t.getClass();
        Field[] fields = clazz.getDeclaredFields();
        int i = 0;
        for (Field field : fields) {
            if (field.getName().equals("id")) {
                continue;
            }
            for (TableEnity enity : tableEnitieList) {
                String column_name = enity.getColumn_name();
                if (StrUtils.lineToHump(column_name).toLowerCase().equals(StrUtils.lineToHump(field.getName()).toLowerCase())) {
                    Method method = clazz.getDeclaredMethod("get" + StrUtils.oneCharUp(field.getName()));
                    Object invoke = method.invoke(t);
                    String value = StrUtils.objectToValue(invoke);
                    if (value != null) {
                        if (i != 0) {
                            sql += ",";
                        }
                        sql +=" " + column_name + " = " + value;
                        i++;
                    }
                }
            }

        }
        sql += " where id = " ;

        for (Field field : fields) {
            if (field.getName().equals("id")) {
                Method method = clazz.getDeclaredMethod("get" + StrUtils.oneCharUp(field.getName()));
                Object invoke = method.invoke(t);
                String value = StrUtils.objectToValue(invoke);
                sql += StrUtils.objectToValue(value);
            }
        }
        logger.info(sql);
        return jdbcTemplate.update(sql);
    }


    /**
     * insert
     * @param t
     * @return
     * @throws Exception
     */
    public int insert(T t) throws Exception {
        Class clazz = t.getClass();
        Annotation annotation = clazz.getAnnotation(Table.class);
        if (annotation == null) {
            String className = clazz.getSimpleName();
            String tableName = StrUtils.humpToLine(className);
            return insert(t, "t" + tableName);
        }else {
            Table table = (Table) annotation;
            String tableName = table.tableName();
            String sql = "insert into " + tableName + " (";

            Field[] fields = clazz.getDeclaredFields();
            boolean flag = false;
            for (int i = 0; i < fields.length; i++) {
                Field field = fields[i];
                if (field.isAnnotationPresent(TableInfo.class)) {
                    TableInfo tableInfo = field.getDeclaredAnnotation(TableInfo.class);
                    String columnComment = tableInfo.columnComment();
                    String columnName = tableInfo.columnName();
                    String columnType = tableInfo.columnType();
                    int autoIncrement = tableInfo.autoIncrement();
                    if (autoIncrement == 1) {
                        continue;
                    }
                    if (flag) {
                        sql +=  "," + columnName;
                    }else {
                        flag = true;
                        sql += columnName;
                    }
                }
            }

            sql += ") values (";

            flag = false;
            for (int i = 0; i < fields.length; i++) {
                Field field = fields[i];
                if (field.isAnnotationPresent(TableInfo.class)) {
                    TableInfo tableInfo = field.getDeclaredAnnotation(TableInfo.class);
                    int autoIncrement = tableInfo.autoIncrement();
                    Method method = clazz.getDeclaredMethod("get" + StrUtils.oneCharUp(field.getName()));
                    Object invoke = method.invoke(t);
                    String value = StrUtils.objectToValue(invoke);
                    if (autoIncrement == 1) {
                        continue;
                    }
                    if (flag) {
                        sql += "," + value;
                    }else {
                        flag = true;
                        sql += value;
                    }
                }
            }
            sql += ")";
            logger.info(sql);
            return jdbcTemplate.update(sql);
        }
    }



    /**
     * 带表名insert  不需要tableinfo注解
     * @param t
     * @param tableName
     * @return
     * @throws Exception
     */
    public int insert(T t , String tableName) throws Exception {
        List<TableEnity> tableEnitieList = getTableInfo(tableName);



        String sql = "insert into " + tableName + " (";
        Class clazz = t.getClass();
        Field[] fields = clazz.getDeclaredFields();
        boolean flag = false;
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            for (TableEnity enity : tableEnitieList) {
                String column_name = enity.getColumn_name();
                if (StrUtils.lineToHump(column_name).toLowerCase().equals(StrUtils.lineToHump(field.getName()).toLowerCase())) {
                    //存在表中的字段
                    if (field.getName().equals("id")) {
                        continue;
                    }
                    if (flag) {
                        sql +=  "," + StrUtils.humpToLine(field.getName());
                    }else {
                        flag = true;
                        sql += StrUtils.humpToLine(field.getName());
                    }
                }
            }
        }
        sql += ") values (";

        flag = false;

        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            for (TableEnity enity : tableEnitieList) {
                String column_name = enity.getColumn_name();
                if (StrUtils.lineToHump(column_name).toLowerCase().equals(StrUtils.lineToHump(field.getName()).toLowerCase())) {
                    if (field.getName().equals("id")) {
                        continue;
                    }
                    Method method = clazz.getDeclaredMethod("get" + StrUtils.oneCharUp(field.getName()));
                    Object invoke = method.invoke(t);
                    String value = StrUtils.objectToValue(invoke);
                    if (flag) {
                        sql += "," + value;
                    }else {
                        flag = true;
                        sql += value;
                    }
                }
            }

        }
        sql += ")";

        //获取下一次自增id
        String getAutoIncrementSql = "SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE   TABLE_NAME='"+tableName+"' AND AUTO_INCREMENT IS NOT NULL";
        Map<String, Object> stringObjectMap = this.findOne(getAutoIncrementSql);
        for (Map.Entry<String, Object> entry : stringObjectMap.entrySet()) {
            Object value = entry.getValue();
            Field id = clazz.getDeclaredField("id");
            id.setAccessible(true);
            if (id.getType().getName().toLowerCase(Locale.ROOT).contains("long")) {
                id.set(t, ((BigInteger) value ).longValue());
            }
            if (id.getType().getName().toLowerCase(Locale.ROOT).contains("integer")) {
                id.set(t, ((BigInteger) value ).intValue());
            }
            if (id.getType().getName().toLowerCase(Locale.ROOT).contains("int")) {
                id.set(t, ((BigInteger) value ).intValue());
            }
        }
        logger.info(sql);
        return jdbcTemplate.update(sql);
    }





    private List<TableEnity> getTableInfo(String tableName) {
        String sql = "SELECT\n" +
                "column_name AS column_name,\n" +
                "data_type AS column_type,\n" +
                "CASE data_type  \n" +
                "WHEN 'bigint' THEN 'Long'\n" +
                "WHEN 'varchar' THEN 'String'\n" +
                "WHEN 'int' THEN 'Integer'\n" +
                "WHEN 'timestamp' THEN 'Date'\n" +
                "WHEN 'datetime' THEN 'Date'\n" +
                "WHEN 'decimal' THEN 'BigDecimal'\n" +
                "END   `data_type`,\n" +
                "character_maximum_length AS `character_maximum_length`,\n" +
                "numeric_precision AS `numeric_precision`,\n" +
                "numeric_scale AS `numeric_scale`,\n" +
                "is_nullable AS `is_nullable`,\n" +
                "CASE WHEN extra = 'auto_increment' THEN 1 ELSE 0 END AS is_auto_increment,\n" +
                "column_default AS column_default,\n" +
                "column_comment AS column_comment\n" +
                "FROM information_schema.columns WHERE table_schema= '"+ dbInfo.getDbName() +"' AND table_name='" + tableName + "'";
        List<TableEnity> tableInfos = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(TableEnity.class));
        return tableInfos;
    }

    public BigDecimal findBigDecimal(String sql, Object[] objects) {
        logger.info(sql);
        logger.info("data = >> " + JSONObject.toJSONString(objects));
        Map<String, Object> one = this.findOne(sql, objects);
        return getBigDecimal(one);
    }


    public BigDecimal findBigDecimal(String sql) {
        logger.info(sql);
        Map<String, Object> one = this.findOne(sql);
        return getBigDecimal(one);
    }







    private BigDecimal getBigDecimal(Map<String, Object> one) {
        for (Map.Entry<String, Object> entry : one.entrySet()) {
            if (entry.getValue() == null){
                return new BigDecimal(0);
            }
            return new BigDecimal(entry.getValue().toString());
        }
        return new BigDecimal(0);
    }


    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

}
