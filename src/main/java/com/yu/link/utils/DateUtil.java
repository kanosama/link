package com.yu.link.utils;

import cn.hutool.core.date.DateTime;
import com.alibaba.fastjson2.JSONObject;
import lombok.SneakyThrows;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

public class DateUtil {


    public static final String FORMAT_0 = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_1 = "yyyy-MM-dd";
    public static final String FORMAT_2 = "HH:mm:ss";
    public static final String FORMAT_3 = "MM/dd/yyyy HH:mm:ss";
    public static final String FORMAT_4 = "yyyyddMMHHmmss";
    public static final String FORMAT_5 = "yyyy-MM";
    public static final String FORMAT_6 = "yyMMddHHmmssSSS";
    public static final String FORMAT_7 = "yyMMdd";
    public static final String FORMAT_8 = "yyyyMMdd";
    public static final String FORMAT_9 = "yyyyMMddHHmmss";
    public static final SimpleDateFormat formatDate = new SimpleDateFormat(FORMAT_1);
    public static final SimpleDateFormat formatDateTime = new SimpleDateFormat(FORMAT_0);
    public static final SimpleDateFormat formatTime = new SimpleDateFormat(FORMAT_2);


    public static final synchronized String getFormatDate() {
        SimpleDateFormat format = new SimpleDateFormat("yyMMddHHmmss");
        return format.format(new Date());
    }

    public static final String getFormatDateSumSeconds(Date date){
        date.setTime(date.getTime()+1000);
        SimpleDateFormat format = new SimpleDateFormat(DateUtil.FORMAT_0);
        return format.format(date);
    }


    public static final synchronized String getFormatStrDate() {
        SimpleDateFormat format = new SimpleDateFormat(FORMAT_0);
        return format.format(new Date());
    }

    public static final synchronized String getFormatDate2Str(String format) {
        SimpleDateFormat formatDate = new SimpleDateFormat(format);
        return formatDate.format(new Date());
    }

    public static final synchronized Date getFormatDate(String fromat) {
        try {
            DateFormat format = new SimpleDateFormat(fromat);

            Date date = format.parse(format.format(new Date()));
            return date;
        }
        catch (Exception e) {

        }
        return null;

    }

    public static final synchronized Date getFormatDate(Date date, String fromat) {
        try {
            DateFormat format = new SimpleDateFormat(fromat);

            return format.parse(format.format(date));
        }
        catch (Exception e) {

        }
        return null;

    }

    /**
     * 计算两个日期之间相差的天数
     * @param sdate 较小的时间
     * @param bdate  较大的时间
     * @return 相差天数
     * @throws Exception
     * calendar 对日期进行时间操作
     * getTimeInMillis() 获取日期的毫秒显示形式
     */
    public static int daysBetween(Date sdate,Date bdate)  {
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdate);
        long time1 = cal.getTimeInMillis();
        cal.setTime(bdate);
        long time2 = cal.getTimeInMillis();
        long between_days=(time2-time1)/(1000*3600*24);
        return Integer.parseInt(String.valueOf(between_days));
    }

    /**
     * 返回当前多少天后的日期
     * @param afterDay 多少天后 负数为多少天前
     * @return
     */
    public static String getAfterDayDate(String pattern, Integer afterDay){
        Calendar calendar1 = Calendar.getInstance();
        SimpleDateFormat sdf1 = new SimpleDateFormat(pattern);
        calendar1.add(Calendar.DATE, afterDay);
        return sdf1.format(calendar1.getTime());
    }

    /**
     *  返回num天后的时间， num为复数则代表几天前
     * @param date 时间
     * @param num
     * @return
     */
    public static Date getDateByNum(Date date , int num){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, num);
        return calendar.getTime();
    }


    /**
     * 返回当前多少天后的日期
     * @param afterDay 多少天后 负数为多少天前
     * @return
     */
    public static String getAfterDayDate(String date , Integer afterDay, String pattern){
        SimpleDateFormat sdf1 = new SimpleDateFormat(pattern);
        Calendar calendar1 = Calendar.getInstance();
        try {
            Date parse = sdf1.parse(date);
            calendar1.setTime(parse);
            calendar1.add(Calendar.DATE, afterDay);
            return sdf1.format(calendar1.getTime());
        } catch (ParseException e) {
            return "";
        }

    }






    /**
     * 计算两个周相差多少周返回list
     * @param week1
     * @param week2
     * @return
     */
    public static List<String> calculateWeeksBetween(String week1, String week2) {
        int year1 = Integer.parseInt(week1.substring(0, 2)) + 2000;
        int year2 = Integer.parseInt(week2.substring(0, 2)) + 2000;
        int weekOfYear1 = Integer.parseInt(week1.substring(2));
        int weekOfYear2 = Integer.parseInt(week2.substring(2));
        int totalWeeks = 0;

        List<String> weeks = new ArrayList<>();
        for (int year = year1; year <= year2; year++) {
            int startWeek = 1;
            int endWeek = 52;

            if (year == year1) {
                startWeek = weekOfYear1;
            }
            if (year == year2) {
                endWeek = weekOfYear2;
            }

            for (int week = startWeek; week <= endWeek; week++) {
                String weekStr = String.format("%02d%02d", year - 2000, week);
                weeks.add(weekStr);
                totalWeeks++;
            }
        }
        System.out.println("Total weeks between " + week1 + " and " + week2 + ": " + totalWeeks);
        return weeks;
    }

    /**
     * 计算两个月相差多少月返回list
     * @return
     */
    public static List<String> calculateMonthsBetween(String month1, String month2) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMM");

        YearMonth date1 = YearMonth.parse(month1, formatter);
        YearMonth date2 = YearMonth.parse(month2, formatter);

        int monthsBetween = (int) ChronoUnit.MONTHS.between(date1, date2) + 1;
        List<String> months = new ArrayList<>();

        for (int i = 0; i < monthsBetween; i++) {
            YearMonth yearMonth = date1.plusMonths(i);
            months.add(yearMonth.format(DateTimeFormatter.ofPattern("yyyyMM")));
        }

        return months;
    }

    /**
     * 计算两日相差多少日返回list
     * @return
     */
    public static List<String> calculateDaysBetween(String date1, String date2) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        LocalDate startDate = LocalDate.parse(date1, formatter);
        LocalDate endDate = LocalDate.parse(date2, formatter);

        List<String> days = new ArrayList<>();
        days.add(startDate.format(formatter));

        while (startDate.isBefore(endDate)) {
            startDate = startDate.plusDays(1);
            days.add(startDate.format(formatter));
        }

        return days;
    }


    public static String getVideoFormat(Long second) {
        int temp = (int) second.longValue();
        int hh = temp / 3600;
        int mm = (temp % 3600) / 60;
        int ss = (temp % 3600) % 60;
        return (hh < 10 ? ("0" + hh) : hh) + ":" +
                (mm < 10 ? ("0" + mm) : mm) + ":" +
                (ss < 10 ? ("0" + ss) : ss);
    }


    public static List<Map<String, String>> splitByDay(String startDateStr, String endDateStr) {
        List<String> dates = DateUtil.calculateDaysBetween(startDateStr, endDateStr);
        List<Map<String, String>> result = new ArrayList<>();
        for (String date : dates) {
            Map<String, String> daysMap = new HashMap<>();
            daysMap.put("type", "day");
            daysMap.put("startDate", date);
            daysMap.put("endDate", date);
            daysMap.put("day", date);
            daysMap.put("days", "1");
            result.add(daysMap);
        }
        return result;
    }


    /**
     * 两个时间  按周切分
     * @return
     */
    public static List<Map<String, String>> splitByWeek(String startDateStr, String endDateStr) {
        LocalDate startDate = LocalDate.parse(startDateStr);
        LocalDate endDate = LocalDate.parse(endDateStr);
        List<Map<String, String>> result = new ArrayList<>();
        LocalDate currentStartDate = startDate;
        while (!currentStartDate.isAfter(endDate)) {
            Map<String, String> weekMap = new HashMap<>();
            LocalDate currentEndDate = currentStartDate.plusDays(6);
            if (currentEndDate.isAfter(endDate)) {
                currentEndDate = endDate;
            }
            weekMap.put("type", "week");
            weekMap.put("startDate", currentStartDate.toString());
            weekMap.put("endDate", currentEndDate.toString());
            Date date = DateTime.of(currentStartDate.toString(), "yyyy-MM-dd").toJdkDate();
            weekMap.put("week", String.valueOf(DateUtil.getTimesWeekNum(date)));
            weekMap.put("year_week", String.valueOf(currentStartDate.getYear()).substring(2, 4) + String.valueOf(DateUtil.getTimesWeekNum(date) < 10 ? "0" + DateUtil.getTimesWeekNum(date) : DateUtil.getTimesWeekNum(date)));
            weekMap.put("days", Integer.toString(currentEndDate.getDayOfYear() - currentStartDate.getDayOfYear() + 1));
            result.add(weekMap);
            currentStartDate = currentEndDate.plusDays(1);
        }

        return result;
    }

    public static int getTimesWeekNum(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);
        calendar.setMinimalDaysInFirstWeek(4);
        return calendar.get(Calendar.WEEK_OF_YEAR);
    }


    /**
     * 两个时间  按月切分
     * @return
     */
    public static List<Map<String, String>> splitByMonth(String startDateStr, String endDateStr) {
        LocalDate startDate = LocalDate.parse(startDateStr);
        LocalDate endDate = LocalDate.parse(endDateStr);
        List<Map<String, String>> result = new ArrayList<>();
        LocalDate currentStartDate = startDate;
        while (!currentStartDate.isAfter(endDate)) {
            Map<String, String> monthMap = new HashMap<>();
            YearMonth currentYearMonth = YearMonth.from(currentStartDate);
            LocalDate currentEndDate = currentYearMonth.atEndOfMonth();
            if (currentEndDate.isAfter(endDate)) {
                currentEndDate = endDate;
            }
            monthMap.put("type", "month");
            monthMap.put("startDate", currentStartDate.toString());
            monthMap.put("endDate", currentEndDate.toString());
            monthMap.put("month", currentYearMonth.toString().replace("-", ""));
            int monthInt = Integer.parseInt(currentYearMonth.toString().replace("-", ""));
            monthMap.put("year_month", String.valueOf(monthInt < 10 ? "0" + monthInt : monthInt));
            monthMap.put("days", Integer.toString(currentEndDate.getDayOfMonth() - currentStartDate.getDayOfMonth() + (currentEndDate.equals(currentStartDate) ? 0 : 1)));
            result.add(monthMap);
            currentStartDate = currentYearMonth.plusMonths(1).atDay(1);
        }

        return result;
    }

    /**
     * 按日期和范围切分小时段
     * @param date 日期
     * @param betweenStr 范围
     * @return
     */
    public static List<Map<String, String>> splitByHour(String date, String betweenStr) {
        String[] split = betweenStr.split("-");
        SimpleDateFormat formatDate = DateUtil.formatDate;
        Integer start = Integer.parseInt(split[0]);
        Integer end = Integer.parseInt(split[1]);
        List<Map<String, String>> result = new ArrayList<>();
        for (int i = start; i <= end ; i++) {
            Map<String, String> hourMap = new HashMap<>();
            hourMap.put("type", "hour");
            hourMap.put("date", date);
            hourMap.put("hour", String.valueOf(i));
            hourMap.put("betweenHour", String.valueOf(i) +"-"+ (i+1));
            result.add(hourMap);
        }
        return result;
    }


    public static void main(String[] args) {
        List<Map<String, String>> maps = splitByMonth("2023-03-01", "2023-03-31");
        System.out.println(JSONObject.toJSONString(maps));
    }


    /**
     * 获取上一周的周一和周日的日期
     * @return
     */
    public static Date[] getLastWeekMondayAndSunday() {
        LocalDate today = LocalDate.now();
        LocalDate lastWeekMonday = today.minusWeeks(1).with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        LocalDate lastWeekSunday = today.minusWeeks(1).with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));
        return new Date[]{localDateToDate(lastWeekMonday), localDateToDate(lastWeekSunday)};
    }



    public static Date localDateToDate(LocalDate localDate) {
        ZonedDateTime zonedDateTime = localDate.atStartOfDay(ZoneId.systemDefault());
        Instant instant = zonedDateTime.toInstant();
        Date date = Date.from(instant);
        return date;
    }


 /**
     * 跟进时间类型，获取开始时间和结束时间
     * @param time 时间
     * @param timeType 1-日，2-周，3-月
     * @param cout 获取时间参数
     * @param isStart 是否是开始时间， 如果是开始时间，会在后面拼接一个00:00:00
     * @return 时间字符串， 格式为yyyy-MM-dd HH:mm:ss
     */
    @SneakyThrows
    public static String beforeByTimeType(String time,Integer timeType,int cout,boolean isStart){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date dateTime = dateFormat.parse(time);
        Calendar calendar= Calendar.getInstance();
        calendar.setTime(dateTime);
        if (timeType == 1 ){
            calendar.add( Calendar.DATE, 0-cout);
            SimpleDateFormat dateF = new SimpleDateFormat("yyyy-MM-dd");
            if (isStart){
                return dateF.format(calendar.getTime())+" 00:00:00";
            }else {
                return dateF.format(calendar.getTime()) + " 23:59:59";
            }
        }else if (timeType ==2){
            calendar.setFirstDayOfWeek(Calendar.MONDAY);
            SimpleDateFormat dateF = new SimpleDateFormat("yyyy-MM-dd");
            if (isStart){
                calendar.add(Calendar.DATE, (0-cout)*7);
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                return dateF.format(calendar.getTime())+" 00:00:00";
            }else {
                calendar.add(Calendar.DATE, (0-cout)*7);
                calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                return dateF.format(calendar.getTime())+" 23:59:59";
            }

        }else {
            calendar.add(Calendar.MONTH, 0-cout);
            SimpleDateFormat dateF = new SimpleDateFormat("yyyy-MM-dd");
            if (isStart){
                calendar.set(Calendar.DAY_OF_MONTH,1);
                return dateF.format(calendar.getTime())+" 00:00:00";
            }else {
                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                return dateF.format(calendar.getTime())+" 23:59:59";
            }

        }
    }


    /**
     * 两个时间相差秒数
     * @param time1 小的时间
     * @param time2 大的时间
     * @return
     */
    public static long betweenDayToSecond(String time1,  String time2){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime1 = LocalDateTime.parse(time1, formatter);
        LocalDateTime dateTime2 = LocalDateTime.parse(time2, formatter);
        Duration duration = Duration.between(dateTime1, dateTime2);
        return Math.abs(duration.getSeconds());
    }


}
