package com.yu.link.filter;

import com.alibaba.fastjson2.JSONObject;
import com.yu.link.entity.AjaxResult;
import com.yu.link.entity.RSAConfig;
import com.yu.link.utils.RSAUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@ControllerAdvice
public class RSAEncryptionResponseBodyAdvice implements ResponseBodyAdvice {

    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        RSAConfig rsaConfig = new RSAConfig();
        if (rsaConfig.getResponseEncryption()) {
            String jsonString = body instanceof String ? body.toString() : JSONObject.toJSONString(body);
            String encrypt = "";
            try {
                encrypt = RSAUtils.encrypt(jsonString);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return encrypt;
        }
        return body;
    }
}
