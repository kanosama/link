package com.yu.link.filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * Web配置 拦截路径
 */
@Configuration
public class WebFilter implements WebMvcConfigurer {


    @Value("${not-interceptors-path}")
    private List<String> pathList;


    /**
     * 添加Web项目的拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(new DecryptInterceptor()).addPathPatterns("/**");

        // 对所有访问路径，都通过TokenRequestInterceptor类型的拦截器进行拦截
        InterceptorRegistration interceptorRegistration = registry.addInterceptor(new TokenRequestInterceptor()).addPathPatterns("/**");
        for (String path: pathList) {
            interceptorRegistration.excludePathPatterns(path);
        }
    }
}

