package com.yu.link.filter;

/**
 * <p> Project: link-start - HttpServletRequestInputStreamFilter
 * <p> create by ly on 2023-10-25 11:24:31
 *
 * @author ly [bearkuku9696@gmail.com]
 * @version 1.0
 */

import com.yu.link.common.InputStreamHttpServletRequestWrapper;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

@Component
@Order(HIGHEST_PRECEDENCE + 1)  // 优先级最高
public class InputStreamHttpServletRequestFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        // 转换为可以多次获取流的request
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        InputStreamHttpServletRequestWrapper inputStreamHttpServletRequestWrapper = new InputStreamHttpServletRequestWrapper(httpServletRequest);
        // 放行
        chain.doFilter(inputStreamHttpServletRequestWrapper, response);
    }
}