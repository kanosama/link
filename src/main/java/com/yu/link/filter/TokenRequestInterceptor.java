package com.yu.link.filter;


import com.alibaba.fastjson2.JSONObject;
import com.yu.link.base.user.entity.TUser;
import com.yu.link.base.user.service.ITUserService;
import com.yu.link.base.web.entity.TokenEntity;
import com.yu.link.entity.AjaxResult;
import com.yu.link.exception.TokenCheckException;
import com.yu.link.utils.RSAUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

@Component
@CrossOrigin
public class TokenRequestInterceptor implements HandlerInterceptor , ApplicationContextAware {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private static  ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext arg0) throws BeansException {
        applicationContext = arg0;
    }



    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURI = request.getRequestURI();
        logger.info("[请求方法]" + requestURI);

        AjaxResult ajaxResult = tokenCheck(request);

        if (ajaxResult.code == 200) {
            return true;
        }else {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json; charset=utf-8");
            PrintWriter out = response.getWriter() ;
            out.write(JSONObject.toJSONString(ajaxResult));
            out.flush();
            out.close();
            return false;
        }

    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }


    private AjaxResult tokenCheck(HttpServletRequest request) throws Exception {
        try {
            ITUserService userService = applicationContext.getBean(ITUserService.class);
            String loginPrivateKey = applicationContext.getEnvironment().resolvePlaceholders("${rsa-private-key}");

            String token = request.getHeader("token");
            if (StringUtils.isBlank(token)) {
                return AjaxResult.create(401, "无权限访问,请登录");
            }
            String tokenJson = RSAUtils.decrypt(token, RSAUtils.getPrivateKey(loginPrivateKey));
            TokenEntity tokenEntity = JSONObject.parseObject(tokenJson, TokenEntity.class);
            String encryptionPassword = tokenEntity.getEncryptionPassword();
            String username = tokenEntity.getUsername();
            TUser user = userService.findUserByUsernameAndPassword(username, encryptionPassword);
            if (user == null) {
                return AjaxResult.create(401, "登录状态变化，请重新登录");
            }
            Integer loginDays = tokenEntity.getLoginDays();
            Long loginTime = tokenEntity.getLoginTime();
            long l = 1000 * 60 * 60 * 24 * loginDays + loginTime;
            if (System.currentTimeMillis() >= l) {
                return AjaxResult.create(401, "登录超时，请重新登录");
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new TokenCheckException(e.getMessage());
        }
        return AjaxResult.success();
    }
}
