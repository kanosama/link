package com.yu.link.aop;

import cn.hutool.crypto.digest.DigestUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.yu.link.annotations.InsteadOfAnnotations;
import com.yu.link.utils.RedisUtils;
import com.yu.link.utils.StrUtils;
import lombok.AllArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

/**
 * <p> Project: link-start - InsteadAspect
 * <p> create by ly on 2023-10-31 09:40:14
 * 方法返回值替换类， redis替换值
 * @author ly [bearkuku9696@gmail.com]
 * @version 1.0
 */
@AllArgsConstructor
@Aspect
@Component
public class RedisInsteadOfAspect {

    private final RedisUtils redisUtils;

    @Around("@annotation(com.yu.link.annotations.InsteadOfAnnotations)")
    public Object redisInsteadOf(ProceedingJoinPoint joinPoint) throws Throwable {
        String methodName = joinPoint.getSignature().getName();
        Object[] args = joinPoint.getArgs();

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        InsteadOfAnnotations annotation = signature.getMethod().getAnnotation(InsteadOfAnnotations.class);
        long time = annotation.time();
        String key = annotation.value();
        boolean forever = annotation.forever();
        Class<?> clazz = annotation.returnClazz();
        if (clazz.equals(InsteadOfAnnotations.class)) {
            clazz = signature.getReturnType();
        }
        String fristKey = methodName;
        String lastKey = args == null ? DigestUtil.md5Hex("null") : DigestUtil.md5Hex(JSONObject.toJSONString(args));
        String redisKey = key + ":" + fristKey + ":" + lastKey;
        if (!redisUtils.exists(redisKey)) {
            Object proceed = joinPoint.proceed();
            if (forever) {
                redisUtils.setValue(redisKey, StrUtils.compressString(JSONObject.toJSONString(proceed)));
            }else {
                redisUtils.setValueTimeout(redisKey, StrUtils.compressString(JSONObject.toJSONString(proceed)), time);
            }
            return proceed;
        }
        String string = redisUtils.getValue(redisKey).toString();
        try {
            return JSONObject.parseObject(StrUtils.decompressString(string), clazz);
        }catch (Exception e){
            return JSONArray.parseArray(StrUtils.decompressString(string)).toList(clazz);
        }
    }
}
