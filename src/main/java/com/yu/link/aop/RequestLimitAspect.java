package com.yu.link.aop;

import com.yu.link.entity.RequestLimit;
import com.yu.link.entity.RequestLimitEntity;
import com.yu.link.utils.BeanUtils;
import com.yu.link.utils.CommonUtils;
import com.yu.link.utils.DateUtil;
import com.yu.link.utils.RedisUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p> Project: link-start - RequestLimitAspect
 * <p> create by ly on 2023-10-24 16:24:06
 *
 * @author ly [bearkuku9696@gmail.com]
 * @version 1.0
 */
@Aspect
@Component
@Order(1)
@Slf4j
public class RequestLimitAspect {

    @Value("${server.servlet.context-path}")
    private String BASE_URL;

    @Autowired
    private RequestLimit requestLimit;

    @Autowired
    private RedisUtils redisUtils;

    @Pointcut("@within(org.springframework.web.bind.annotation.RestController) && @annotation(com.yu.link.annotations.RequestLimit)")
    public void pointcut() {
    }

    @Around("pointcut()")
    @SneakyThrows
    public Object method(ProceedingJoinPoint joinPoint)  {

        // 获得request对象
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();

        String ip = CommonUtils.getIpAddr(request);
        String url = request.getRequestURL().toString();
        String lastUrl = url.split(BASE_URL)[1];
        SimpleDateFormat format = DateUtil.formatDateTime;
        String key = ip+":"+lastUrl;
        if (!redisUtils.exists(key)) {
            setFirstRedis(ip, url, key);
        }else {
            Object value = redisUtils.getValue(key);
            RequestLimitEntity convert = BeanUtils.convert(value, RequestLimitEntity.class);
            String firstDateTime = convert.getFirstDateTime();
            String lastDateTime = convert.getFirstDateTime();
            long counts = convert.getCounts();
            if (counts > requestLimit.getCount()) {
                throw new RuntimeException("访问过于频繁，请稍后重试");
            }
            long watis = requestLimit.getWatis();
            long time = requestLimit.getTime();
            Date now = new Date();
            if (DateUtil.betweenDayToSecond(firstDateTime, format.format(now)) >= time) {
                setFirstRedis(ip, url, lastUrl);
            }else {
                if (DateUtil.betweenDayToSecond(lastDateTime, format.format(now)) < watis) {
                    throw new RuntimeException("访问过于频繁，请稍后重试");
                }else {
                    redisUtils.delKey(key);
                    convert.setLastDateTime(format.format(new Date()));
                    counts++;
                    convert.setCounts(counts);
                    redisUtils.setValue(key, convert);
                    redisUtils.expire(key, time);
                }
            }
        }

        // result的值就是被拦截方法的返回值
        try {
            Object result = joinPoint.proceed();
            return result;
        }catch (Exception E){
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return null;
    }

    private void setFirstRedis(String ip, String url, String key){
        redisUtils.delKey(key);
        SimpleDateFormat format = DateUtil.formatDateTime;
        RequestLimitEntity requestLimitEntity = new RequestLimitEntity();
        requestLimitEntity.setIp(ip);
        requestLimitEntity.setUrl(url);
        Date date = new Date();
        requestLimitEntity.setFirstDateTime(format.format(date));
        requestLimitEntity.setLastDateTime(format.format(date));
        requestLimitEntity.setCounts(1);
        redisUtils.setValue(key, requestLimitEntity);
        long time = requestLimit.getTime();
        redisUtils.expire(key, time);
    }

}
