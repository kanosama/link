package com.yu.link.base.user.controller;

import com.yu.link.base.user.service.ITUserService;
import com.yu.link.entity.AjaxResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private ITUserService userService;

    @GetMapping("/userInfo")
    public AjaxResult getUserInfo(HttpServletRequest request) throws Exception {
        String token = request.getHeader("token");
        if (StringUtils.isNotBlank(token)) {
            return AjaxResult.success("查询成功", userService.getUserInfoByToken(token));
        }else {
            return AjaxResult.success("未登录", "");
        }

    }

}
