package com.yu.link.base.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yu.link.base.user.entity.TUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2023-10-26
 */
public interface ITUserService extends IService<TUser> {

    TUser getUserInfoByToken(String token);

    TUser findUserByUsername(String username);

    TUser findUserByUsernameAndPassword(String username, String encryptionPassword);
}
