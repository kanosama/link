package com.yu.link.base.user.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yu.link.base.user.entity.TUser;
import com.yu.link.base.user.entity.resp.GetLowerUserResp;
import com.yu.link.base.user.mapper.TUserMapper;
import com.yu.link.base.user.service.ITUserService;
import com.yu.link.base.web.entity.TokenEntity;
import com.yu.link.entity.RSAConfig;
import com.yu.link.utils.RSAUtils;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2023-10-26
 */
@Service
@AllArgsConstructor
public class TUserServiceImpl extends ServiceImpl<TUserMapper, TUser> implements ITUserService {

    private TUserMapper tUserMapper;

    public TUser findUserByUsername(String username) {
        return tUserMapper.selectOne(new QueryWrapper<TUser>().eq("username", username));
    }

    @Override
    public TUser findUserByUsernameAndPassword(String username, String encryptionPassword) {
        return this.findUserByUsernameAndPassword(username, encryptionPassword);
    }


    public Integer insert(TUser user) throws Exception {
        return tUserMapper.insert(user);
    }

    public TUser getUserInfoByRequest(HttpServletRequest request) throws Exception {
        String token = request.getHeader("token");
        return getUserInfoByToken(token);
    }

    @SneakyThrows
    public TUser getUserInfoByToken(String token)  {
        RSAConfig rsaConfig = new RSAConfig();
        String tokenJson = RSAUtils.decrypt(token, RSAUtils.getPrivateKey(rsaConfig.getPrivateKey()));
        TokenEntity tokenEntity = JSONObject.parseObject(tokenJson, TokenEntity.class);
        Integer userId = tokenEntity.getUserId();
        TUser user = tUserMapper.selectById(userId);
        user.setPasswrod("");
        user.setSalt("");
        return user;
    }
}
