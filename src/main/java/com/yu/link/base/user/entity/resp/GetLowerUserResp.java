package com.yu.link.base.user.entity.resp;

import lombok.Data;


@Data
public class GetLowerUserResp {

    private Integer userId;
    private String userName;
}
