package com.yu.link.base.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yu.link.base.user.entity.TUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2023-10-26
 */
public interface TUserMapper extends BaseMapper<TUser> {

}
