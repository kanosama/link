package com.yu.link.base.web.entity.req;


import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RegisterReq {

    @NotBlank(message = "用户名不能为空")
    private String username;
    @NotBlank(message = "密码不能为空")
    private String password;
//    @NotBlank(message = "邀请码不能为空")
//    private String invitationCode;

}
