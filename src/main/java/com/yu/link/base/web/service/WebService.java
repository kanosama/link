package com.yu.link.base.web.service;

import com.alibaba.fastjson2.JSONObject;
import com.yu.link.base.user.entity.TUser;
import com.yu.link.base.user.service.ITUserService;
import com.yu.link.base.web.entity.TokenEntity;
import com.yu.link.base.web.entity.req.LoginReq;
import com.yu.link.base.web.entity.req.RegisterReq;
import com.yu.link.entity.AjaxResult;
import com.yu.link.entity.ParamException;
import com.yu.link.utils.AESUtils;
import com.yu.link.utils.Md5Utils;
import com.yu.link.utils.RSAUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class WebService {


    @Value("${loginDays}")
    private long loginDays;

    @Autowired
    private ITUserService userService;

    public AjaxResult login(LoginReq loginReq) throws Exception {
        String username = loginReq.getUsername();
        TUser user = userService.findUserByUsername(username);
        if (user == null) {
            throw new IllegalArgumentException("用户名或者密码不正确");
        }
        String password = loginReq.getPassword();
        String salt = AESUtils.encrypt(password);
        if (!Md5Utils.hash(salt).equals(user.getPasswrod())) {
            throw new IllegalArgumentException("用户名或者密码不正确");
        }

        user.setPasswrod("");
        user.setSalt("");
        //登录成功 生成token
        long loginTime = System.currentTimeMillis();
        TokenEntity tokenEntity = new TokenEntity();
        tokenEntity.setLoginTime(loginTime);
        tokenEntity.setLoginDays(Integer.parseInt(String.valueOf(loginDays)));
        tokenEntity.setUsername(username);
        tokenEntity.setUserId(user.getId());
        tokenEntity.setEncryptionPassword( Md5Utils.hash(salt));
        String token = RSAUtils.encrypt(JSONObject.toJSONString(tokenEntity));

        HashMap<String, Object> data = new HashMap<>();
        data.put("user", user);
        data.put("token", token);
        return AjaxResult.success("登录成功", data);
    }

    public void register(RegisterReq registerReq) throws Exception {
        String password = registerReq.getPassword();
        String username = registerReq.getUsername();
//        String invitationCode = registerReq.getInvitationCode();
        TUser userByUsername = userService.findUserByUsername(username);
        if (userByUsername != null) {
            throw new ParamException("用户名已被注册!").code(406);
        }
        String salt = AESUtils.encrypt(password);
        String encryptPassword = Md5Utils.hash(salt);
        TUser user = new TUser();
        user.setUsername(username);
        user.setPasswrod(encryptPassword);
        user.setSalt(salt);
        user.setStatus(1);
        userService.save(user);
    }

}
