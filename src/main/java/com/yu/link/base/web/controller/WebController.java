package com.yu.link.base.web.controller;

import com.yu.link.base.user.service.ITUserService;
import com.yu.link.base.web.entity.req.LoginReq;
import com.yu.link.base.web.entity.req.RegisterReq;
import com.yu.link.base.web.service.WebService;
import com.yu.link.entity.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class WebController {

    @Autowired
    private WebService webService;

    @GetMapping("/login")
    public AjaxResult login(@Validated LoginReq loginReq) throws Exception {
        AjaxResult ajaxResult = webService.login(loginReq);
        return ajaxResult;
    }


    @GetMapping("/register")
    public AjaxResult register(@Validated RegisterReq registerReq) throws Exception {
        webService.register(registerReq);
        return AjaxResult.success("注册成功");
    }
}
