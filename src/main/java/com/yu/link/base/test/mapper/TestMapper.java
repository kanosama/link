package com.yu.link.base.test.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yu.link.base.test.entity.Test;

public interface TestMapper extends BaseMapper<Test> {
}
