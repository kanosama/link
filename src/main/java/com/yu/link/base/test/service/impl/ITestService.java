package com.yu.link.base.test.service.impl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yu.link.base.test.entity.Test;

public interface ITestService extends IService<Test> {
}
