package com.yu.link.base.test.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * <p> Project: link-start-master - Test
 * <p> create by ly on 2023-10-20 17:37:00
 *
 * @author ly [bearkuku9696@gmail.com]
 * @version 1.0
 */
@TableName("test")
@Data
public class Test {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id ;
    private String test;
}
