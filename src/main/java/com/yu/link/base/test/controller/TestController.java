package com.yu.link.base.test.controller;

import com.yu.link.annotations.RequestLimit;
import com.yu.link.base.test.entity.Test;
import com.yu.link.base.test.service.impl.ITestService;
import com.yu.link.common.CodeGenerator;
import com.yu.link.entity.AjaxResult;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p> Project: link-start-master - TestController
 * <p> create by ly on 2023-10-20 17:43:02
 *
 * @author ly [bearkuku9696@gmail.com]
 * @version 1.0
 */
@Api(tags = "测试")
@RestController
@RequestMapping("TEST")
@AllArgsConstructor
public class TestController {

    private final ITestService testService;

    @GetMapping("add")
    @RequestLimit
    public AjaxResult add(){
        Test test = new Test();
        test.setTest("测试");
        testService.save(test);
        return AjaxResult.success();
    }


}
