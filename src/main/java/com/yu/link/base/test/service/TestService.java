package com.yu.link.base.test.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yu.link.base.test.entity.Test;
import com.yu.link.base.test.mapper.TestMapper;
import com.yu.link.base.test.service.impl.ITestService;
import org.springframework.stereotype.Service;

/**
 * <p> Project: link-start-master - TestService
 * <p> create by ly on 2023-10-20 17:44:23
 *
 * @author ly [bearkuku9696@gmail.com]
 * @version 1.0
 */
@Service
public class TestService extends ServiceImpl<TestMapper , Test> implements ITestService {
}
