package com.yu.link.annotations;

import java.lang.annotation.*;

/**
 * <p> Project: link-start - InsteadOfAnnotations
 * <p> create by ly on 2023-10-31 09:40:54
 * 替代注解
 * @author ly [bearkuku9696@gmail.com]
 * @version 1.0
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface InsteadOfAnnotations {
    String value() default "insteadOf";
    long time() default 60;
    boolean forever() default false;
    Class<?> returnClazz() default InsteadOfAnnotations.class;
}
