package com.yu.link.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 实体类字段注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface TableInfo {

    /**
     * 字段名
     */
    public String columnName();
    /**
     * 字段类型
     */
    public String columnType() default "";
    /**
     * 是否自增
     */
    public int autoIncrement() default 0;
    /**
     * 备注
     */
    public String columnComment() default "";



}
