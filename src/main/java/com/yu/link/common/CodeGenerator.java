package com.yu.link.common;

/**
 * <p> Project: link-start - CodeGenerator
 * <p> create by ly on 2023-10-26 11:32:33
 * 代码生成器
 * @author ly [bearkuku9696@gmail.com]
 * @version 1.0
 */

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.yu.link.entity.DataSource;
import com.yu.link.utils.StrUtils;
import lombok.SneakyThrows;

public class CodeGenerator {

    private static final String BASE_PATH = "com.yu.link.base";

    @SneakyThrows
    public static void generateByTables(String tableNames) {
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        DataSource dataSource = new DataSource();
        dataSourceConfig.setUrl(dataSource.getUrl());
        dataSourceConfig.setDriverName(dataSource.getDriverClassName());
        dataSourceConfig.setUsername(dataSource.getUsername());
        dataSourceConfig.setPassword(dataSource.getPssword());
        GlobalConfig globalConfig = new GlobalConfig();
        StrategyConfig strategyConfig = new StrategyConfig();
        strategyConfig.setNaming(NamingStrategy.underline_to_camel);
        strategyConfig.setColumnNaming(NamingStrategy.underline_to_camel);
        strategyConfig.setEntityLombokModel(true);
        strategyConfig.setRestControllerStyle(true);
        strategyConfig.setControllerMappingHyphenStyle(true);	    //驼峰转连字符
        strategyConfig.setInclude(tableNames);
        String className = StrUtils.lineToHump(tableNames);
        PackageConfig packageConfig = new PackageConfig();
        packageConfig.setParent(BASE_PATH + "/" + className);
        packageConfig.setController("controller");
        packageConfig.setService("service");
        packageConfig.setServiceImpl("service.impl");
        packageConfig.setEntity("entity");
        packageConfig.setMapper("mapper");
        AutoGenerator autoGenerator = new AutoGenerator();
        autoGenerator.setDataSource(dataSourceConfig);
        autoGenerator.setGlobalConfig(globalConfig);
        autoGenerator.setStrategy(strategyConfig);
        autoGenerator.setPackageInfo(packageConfig);
        autoGenerator.setDataSource(dataSourceConfig);
        autoGenerator.execute();
    }
}
