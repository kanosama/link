package com.yu.link.exception;

import com.yu.link.entity.AjaxResult;
import com.yu.link.entity.ParamException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ExceptionHandler(value = Exception.class)
    public AjaxResult exceptionHandler(HttpServletRequest request, Exception e) {
        String message = e.getMessage();
        logger.error(message);
        if (e instanceof org.springframework.validation.BindException) {
            return AjaxResult.create(406, message.substring(message.lastIndexOf("[" )+ 1, message.lastIndexOf("]")));
        }
        if (e instanceof ParamException) {
            return AjaxResult.create(((ParamException)e ).getCode(), e.getMessage());
        }
        return AjaxResult.error(message);
    }
}
