package com.yu.link.exception;

public class TokenCheckException extends Exception{


    public TokenCheckException() {
        super();
    }

    public TokenCheckException(String message) {
        super(message);
    }

    public TokenCheckException(String message, Throwable cause) {
        super(message, cause);
    }

    public TokenCheckException(Throwable cause) {
        super(cause);
    }

    protected TokenCheckException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
