package com.yu.link;

import com.yu.link.common.CodeGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class LinkStartApplicationTests {

    @Test
    void contextLoads() {
        CodeGenerator.generateByTables("t_user");
    }

}
